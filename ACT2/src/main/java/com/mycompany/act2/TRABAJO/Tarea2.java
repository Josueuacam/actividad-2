
package com.mycompany.act2.TRABAJO;

/**
 *
 * @author SOLIS 
 */
public class Tarea2 {
    
}
//Array Of Multiples
import java.util.stream.IntStream;
public class Program {
	public static int[] arrayOfMultiples(int num, int length) {
		return IntStream.rangeClosed(1, length)
			.map(i -> num * i)
			.toArray();
	}
}

//Perfect square patch
public class Challenge {
	public static int[][] squarePatch(int n) {
		int[][] finalArray = new int[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				finalArray[i][j] = n;
			}
		}
		return finalArray;
	}
}
//Seven Boom
import java.util.Arrays;
public class Challenge {
	public static String sevenBoom(int[] arr) {
		return Arrays.toString(arr).contains("7") ? "Boom!" 
			: "there is no 7 in the array"; 
	}
}
//Pair Management
public class Pairs {
	public static int[] makePair(int a, int b) {
		return new int[] {a, b};
	}
}
//Fruit Salad
public class Fruits {
	public static String fruitSalad(String[] fruits) {
		int i;
		int len = fruits.length;
		String fruit;
		int fruitLen;
		String[] slices = new String[len * 2];
		for (i = 0; i < len; i++) {
			fruit = fruits[i];
			fruitLen = fruit.length();
			slices[2 * i] = fruit.substring(0, fruitLen / 2);
			slices[2 * i + 1] = fruit.substring(fruitLen / 2);
		}
		java.util.Arrays.sort(slices);
		String s = "";
		for (String slice : slices) s += slice;
		return s;
	}
}
